resource "aws_iam_policy" "lambda-policy" {
  name        = "ebs-backup-policy"
  path        = "/"
  description = "Policy for EBS backup Lambda"
  policy      = file("${path.module}/files/policy.json")
}

resource "aws_iam_role" "lambda-role" {
  assume_role_policy = file("${path.module}/files/role.json")
}

resource "aws_iam_role_policy_attachment" "lambda-attach" {
  role       = aws_iam_role.lambda-role.name
  policy_arn = aws_iam_policy.lambda-policy.arn
}

resource "aws_cloudwatch_event_rule" "ebs-snapshot-schedule" {
  name                = "ebs-snapshot-schedule"
  description         = "Schedule EBS snapshot"
  schedule_expression = var.schedule_expression
  is_enabled          = var.schedule_enabled
}

resource "aws_cloudwatch_event_target" "lambda" {
  rule = aws_cloudwatch_event_rule.ebs-snapshot-schedule.name
  arn  = aws_lambda_function.ebs-snapshot.arn
}

resource "aws_lambda_permission" "ebs-backup" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.ebs-snapshot.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.ebs-snapshot-schedule.arn
}

resource "aws_lambda_function" "ebs-snapshot" {
  function_name    = "ebs-snapshot"
  runtime          = "python3.6"
  timeout          = 10
  role             = aws_iam_role.lambda-role.arn
  handler          = "ebs-snapshot.ebs_snapshot"
  source_code_hash = filebase64sha256("${path.module}/files/ebs-snapshot.zip")
  filename         = "${substr(path.module, length(path.cwd) + 1, -1)}/files/ebs-snapshot.zip"
}

# Terraform module for ebs-backup Lambda function

You need to create tag `Backup` for ebs snapshot creation.
If `Backup_Devices` tag is not specified, for **all** ebs volumes
attached to ec2 instance with `Backup` tag, snapshot will be created.

tag examples

| Name | Value |
|------|-------|
| Backup | Yes |
| Backup_Devices | /dev/sda1,/dev/sdb |

## Usage example

```hcl
provider "aws" {
  region = "eu-west-1"
}

module "ebs-backup" {
  source = "../module/ebs-backup"

}
```
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| schedule_enabled | Backup EBS Lambda schedule enabled | string | `true` | no |
| schedule_expression | Backup EBS Lambda schedule cron expression<br\>  *i.e.:* 'cron(0 1 * * ? *)' run every day at 1am | string | `cron(0 1 * * ? *)` | no |

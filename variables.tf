variable "schedule_expression" {
  type = string

  description = <<EOP
Backup EBS Lambda schedule cron expression<br\>
 *i.e.:* 'cron(0 1 * * ? *)' run every day at 1am
EOP

  default = "cron(0 1 * * ? *)"
}

variable "schedule_enabled" {
  description = "Backup EBS Lambda schedule enabled"
  default     = true
}

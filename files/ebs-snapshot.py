# -*- coding: utf-8 -*-
# pylint: disable=C0103
"""Lambda function to create snapshot of ebs volumes"""

import os
import logging
from datetime import datetime, timezone, timedelta
import boto3

# backup delete
backup_delete_after = 14
if 'DELETE_AFTER' in os.environ:
    backup_delete_after = os.environ['DELETE_AFTER']

# get logger
logger = logging.getLogger(__name__)
if 'DEBUG' in os.environ:
    logger.setLevel(logging.INFO)
else:
    logger.setLevel(logging.DEBUG)


def log(message):
    """log info message"""
    logger.info(message)


def debug(message):
    """log debug message"""
    logger.debug(message)


def ebs_snapshot(event, context):
    '''Main lambda function'''
    ec2 = boto3.resource('ec2')

    # filter instances with tag:Backup
    instance_filter = [
        {
            "Name": "tag:Backup",
            "Values": ['Yes', 'yes']
        },
        {
            "Name": "instance-state-name",
            "Values": ['running']
        }
    ]
    instances = ec2.instances.filter(Filters=instance_filter)

    # loop trough filtered instances
    print(type(instances))
    if list(instances):
        backup_volumes_ids = []
        for instance in instances:
            log("Running on instance {}".format(instance.instance_id))
            for tag in instance.tags:
                if tag['Key'] == 'Backup_Devices':
                    devices = [x.strip() for x in tag['Value'].split(',')]
                    debug('tag:Backup_Devices - {}'.format(devices))
                    for volume in instance.volumes.all():
                        for attach in volume.attachments:
                            if attach['Device'] in devices:
                                backup_volumes_ids.append(attach['VolumeId'])
            # if tag Backup_Devices is not set backup all volumes
            if not backup_volumes_ids:
                backup_volumes_ids.extend(
                    [volume.id for volume in instance.volumes.all()])
            for volume in backup_volumes_ids:
                # backup ebs volumes
                log('Creating snapshot for volume {} '.format(volume))
                #snapshot = ec2.Volume(volume).create_snapshot()
                # delete old volume snapshots
                snapshot_filter = [
                    {
                        'Name': 'volume-id',
                        'Values' : [volume]
                    }
                ]
                snapshots = ec2.snapshots.filter(Filters=snapshot_filter)
                if snapshots:
                    for snap in snapshots:
                        start_time = (snap.start_time)
                        delete_time = datetime.now(tz=timezone.utc) - timedelta(days=backup_delete_after)
                        if start_time < delete_time:
                            log('Deleting old volume snapshot {}'.format(snap.snapshot_id))
                            snap.delete()
    else:
        log('No instances found by filter.')

if __name__ == '__main__':
    logformat = "%(asctime)s %(levelname)s %(module)s: %(message)s"
    datefmt = "%d-%m-%Y %H:%M"
    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(logging.Formatter(fmt=logformat, datefmt=datefmt))
    logger.addHandler(stream_handler)

    ebs_snapshot('event', 'context')
